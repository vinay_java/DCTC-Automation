import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorsInSeleniumSiblings {

	//created a function/method without parameter
	public void setup() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("https://demoqa.com/webtables");
		
		WebElement div_info=driver.findElement(By.xpath("//div[contains(text(),'Insurance')]//preceding-sibling::div[2]"));
		System.out.println(div_info.getText());
		
		
		WebElement div_info1=driver.findElement(By.xpath("//div[contains(text(),'Vega')]//following-sibling::div[1]"));
		System.out.println(div_info1.getText());
		
		
	}
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		LocatorsInSeleniumSiblings loc=new LocatorsInSeleniumSiblings(); //create an object of class
		loc.setup();
	}
	

}
