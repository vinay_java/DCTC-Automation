import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ActionsDemo {

	WebDriver driver;		//class variable
	String path="";
	String actual_driver_path="";
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		ActionsDemo ad=new ActionsDemo();
		ad.setup();
		ad.right_click_on_button();
		ad.click_on_submenu();
	}
	
	public void setup()
	{
		path=System.getProperty("user.dir");
		actual_driver_path=path+"\\Driver\\chromedriver.exe";
		
		System.setProperty("web.chrome.driver", actual_driver_path);
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.guru99.com/test/simple_context_menu.html");
	}
	
	public void right_click_on_button()
	{
		WebElement btn_right_click=driver.findElement(By.xpath("//span[contains(text(),'right click me')]"));
		Actions act=new Actions(driver);
		act.contextClick(btn_right_click).build().perform();
	}
	public void click_on_submenu() throws InterruptedException
	{
		WebElement edit_btn=driver.findElement(By.xpath("//span[contains(text(),'Edit')]"));
		edit_btn.click();
		handle_alert();
	}
	
	public void handle_alert() throws InterruptedException
	{
		Alert alt=driver.switchTo().alert();
		Thread.sleep(3000);
		System.out.println(alt.getText());
		alt.accept();
	}
}











