import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestAutomationFirefox {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		System.setProperty("webdriver.gecko.driver","C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\geckodriver.exe");
		WebDriver driver=new FirefoxDriver();	//Object of WebDriver
		driver.manage().window().maximize();	//maximize the browser
		
		driver.get("https://www.facebook.com/");	//Will navigate to desired URL
		driver.close();							//Close the Browser
	}
}
