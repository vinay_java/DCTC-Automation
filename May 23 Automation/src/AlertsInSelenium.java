import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertsInSelenium {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		AlertsInSelenium al=new AlertsInSelenium();
		al.getAlert();
		al.getAlertCancle();
		al.getAlertTextfield();
	}

	public void getAlert() throws InterruptedException
	{
		System.setProperty("web.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/alerts");
		
		WebElement btn_click= driver.findElement(By.id("alertButton"));
		btn_click.click();

		Alert alt=driver.switchTo().alert();			//driver switches to alert
		Thread.sleep(3000);
		System.out.println(alt.getText());
		alt.accept();									//click on Ok Button of Alert
		driver.close();
	}
	
	public void getAlertCancle() throws InterruptedException
	{
		System.setProperty("web.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/alerts");
		
		WebElement btn_click= driver.findElement(By.id("confirmButton"));
		btn_click.click();

		Alert alt=driver.switchTo().alert();			//driver switches to alert
		Thread.sleep(3000);
		System.out.println(alt.getText());
		alt.dismiss();									//click on Cancel Button of Alert
		driver.close();
	}
	
	public void getAlertTextfield() throws InterruptedException
	{
		System.setProperty("web.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/alerts");
		
		WebElement btn_click= driver.findElement(By.id("promtButton"));
		btn_click.click();

		Alert alt=driver.switchTo().alert();			//driver switches to alert
		Thread.sleep(3000);
		System.out.println(alt.getText());
		alt.sendKeys("Ajay");							//we are seding text in Textfield of Alert
		alt.accept();									//click on Ok Button of Alert
		
		Thread.sleep(3000);
		driver.close();
	}
	
}





