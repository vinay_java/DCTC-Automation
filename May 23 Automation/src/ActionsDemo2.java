import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ActionsDemo2 {

	WebDriver driver;		//class variable
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		ActionsDemo2 ad=new ActionsDemo2();
		ad.setup();
		ad.double_click_on_button();
	}
	
	public void setup()
	{
		System.setProperty("web.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.guru99.com/test/simple_context_menu.html");
	}
	
	public void double_click_on_button() throws InterruptedException
	{
		WebElement btn_double_click=driver.findElement(By.xpath("//button[contains(text(),'Double-Click Me To See Alert')]"));
		
		Actions act=new Actions(driver);
		act.doubleClick(btn_double_click).build().perform();
		
		handle_alert();
	}
	
	public void handle_alert() throws InterruptedException
	{
		Alert alt=driver.switchTo().alert();
		Thread.sleep(3000);
		System.out.println(alt.getText());
		alt.accept();
	}
}











