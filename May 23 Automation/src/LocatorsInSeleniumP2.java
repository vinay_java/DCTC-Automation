import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorsInSeleniumP2 {

	//created a function/method without parameter
	public void setup()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("https://www.facebook.com/");
		
		WebElement link_forgotpwd=driver.findElement(By.linkText("Forgotten password?"));	//locator - linkText
		link_forgotpwd.click();
		
		WebElement link_partial_mr=driver.findElement(By.partialLinkText("Contact")); //locator - partialLinkText
		link_partial_mr.click();
		
		WebElement txt_email=driver.findElement(By.tagName("input")); //locator - tagName
		
		//WebElement txt_email=driver.findElement(By.id("email"));
		txt_email.sendKeys("test user name");
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LocatorsInSeleniumP2 loc=new LocatorsInSeleniumP2(); //create an object of class
		loc.setup();
	}
	

}
