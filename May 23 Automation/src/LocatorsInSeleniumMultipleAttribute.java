import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorsInSeleniumMultipleAttribute {

	//created a function/method without parameter
	public void setup() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("https://www.facebook.com/");
		
		
		//relative xpath --- Multiple Attributes
		WebElement link=driver.findElement(By.xpath("//a[@dir='ltr' and @title='Hindi']"));
		System.out.println(link.getText());		
		
		WebElement link_v1= driver.findElement(By.xpath("//a[@title='See the Voting Information Centre']"));
		System.out.println(link_v1.getText());
		System.out.print(link_v1.getAttribute("href"));
		link_v1.click();
	}
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		LocatorsInSeleniumMultipleAttribute loc=new LocatorsInSeleniumMultipleAttribute(); //create an object of class
		loc.setup();
	}
	

}
