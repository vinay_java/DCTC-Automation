import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorsInSeleniumP4 {

	WebDriver driver;		//class variable
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LocatorsInSeleniumP4 p4=new LocatorsInSeleniumP4();
		p4.setup();
		p4.getLocator();
	}
	
	public void setup()
	{
		System.setProperty("web.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
	}
	
	public void getLocator()
	{
		driver.get("https://www.flipkart.com/");
		WebElement tag_a= driver.findElement(By.tagName("a"));
		System.out.println(tag_a.getAttribute("title"));
		
		List<WebElement> tag_multi = driver.findElements(By.tagName("a"));
		System.out.println(tag_multi.size());
		
		for(WebElement x:tag_multi)
		{
			System.out.println(x.getAttribute("title")+" : "+x.getAttribute("href"));
		}
	}
}