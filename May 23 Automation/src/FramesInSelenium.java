import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FramesInSelenium {

	WebDriver driver;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		FramesInSelenium f=new FramesInSelenium();
		f.setup();
		f.locate_frame_and_switch_driver();
		f.locate_link();
	}
	
	public void setup()
	{
		System.setProperty("web.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.studentstutorial.com/code-editor/?topic=html&file_name=create_submenu");
	}

	public void locate_frame_and_switch_driver()
	{
		WebElement frame_code=driver.findElement(By.id("code"));
		driver.switchTo().frame(frame_code);			
	}
	public void locate_link()
	{
		WebElement link=driver.findElement(By.linkText("Main Item 3"));
		System.out.println(link.getText());
		link.click();
	}
}






