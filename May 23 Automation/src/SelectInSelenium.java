import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

public class SelectInSelenium {


	
	public void getSelect() throws InterruptedException
	{
		System.setProperty("web.chrome.driver","C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.guru99.com/test/newtours/register.php");
		
		//Select tag for Country
		WebElement sel_tag=driver.findElement(By.name("country"));
		
		Select sel=new Select(sel_tag);
		Thread.sleep(3000);
		sel.selectByIndex(12);
		Thread.sleep(3000);
		sel.selectByValue("KOREA, REPUBLIC OF");			//value available for value attribute
		Thread.sleep(3000);
		sel.selectByVisibleText("COLOMBIA");
		
		Thread.sleep(3000);
		driver.get("https://demoqa.com/select-menu");
		
		//Select tag for Color
		
		WebElement sel_tag1= driver.findElement(By.id("oldSelectMenu"));
		Select sel_color=new Select(sel_tag1);
		
		Thread.sleep(3000);
		sel_color.selectByValue("3");
		
		System.out.println(sel_color.isMultiple());		//checking whether we can select multiple options
		
		//Select tag for Car
		WebElement sel_tag2=driver.findElement(By.id("cars"));
		Select sel_cars=new Select(sel_tag2);
		
		sel_cars.selectByVisibleText("Opel");
		sel_cars.selectByIndex(0);
		sel_cars.selectByValue("saab");
		System.out.println(sel_cars.isMultiple());
		
		Thread.sleep(3000);
		sel_cars.deselectByValue("saab");
		
		Thread.sleep(3000);
		sel_cars.deselectAll();
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		SelectInSelenium s1=new SelectInSelenium();
		s1.getSelect();
	}

}
