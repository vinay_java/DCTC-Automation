import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class NavigationInSelenium {

	WebDriver driver;		//class variable
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		NavigationInSelenium n=new NavigationInSelenium();
		n.setup();
		n.getNavigation();
	}
	
	public void setup()
	{
		System.setProperty("web.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
	}
	
	public void getNavigation() throws InterruptedException
	{
		driver.get("https://www.facebook.com/");
		WebElement tag_a= driver.findElement(By.linkText("Forgotten password?"));
		tag_a.click();
		
		Thread.sleep(3000);
		driver.navigate().back();
		
		Thread.sleep(3000);
		driver.navigate().forward();

		Thread.sleep(3000);
		driver.navigate().refresh();
		
		Thread.sleep(3000);
		driver.navigate().to("https://www.google.com/");
	}
}










