import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckboxButtonInSelenium {


	public void getCheckbox() throws InterruptedException
	{
		System.setProperty("web.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://demo.guru99.com/test/radio.html");
		
		WebElement check1= driver.findElement(By.id("vfb-6-0"));
		System.out.println(check1.getAttribute("value"));
		//isDisplayed -- when the webelement is displayed
		System.out.println("checkbox 1 Displayed : "+check1.isDisplayed());
		
		//isEnabled -- checks if the web element is clickable
		//isEnabled --- false -- if web element is not clickable
		System.out.println("checkbox 1 Enabled : "+check1.isEnabled());		
		
		//isSelected -- works when we click on webelement
		System.out.println("checkbox 1 Before :"+check1.isSelected());		//false
		check1.click();
		System.out.println("checkbox 1 After :"+check1.isSelected());		//true
		
		
		WebElement check2= driver.findElement(By.id("vfb-6-1"));
		System.out.println(check2.getAttribute("value"));
		//isDisplayed -- when the webelement is displayed
		System.out.println("checkbox 2 Displayed : "+check2.isDisplayed());
		
		//isEnabled -- checks if the web element is clickable
		//isEnabled --- false -- if web element is not clickable
		System.out.println("checkbox 2 Enabled : "+check2.isEnabled());		
		
		//isSelected -- works when we click on webelement
		System.out.println("checkbox 2 Before :"+check2.isSelected());		//false
		check2.click();
		System.out.println("checkbox 2 After :"+check2.isSelected());		//true
		
		
		WebElement check3= driver.findElement(By.id("vfb-6-2"));
		System.out.println(check3.getAttribute("value"));
		//isDisplayed -- when the webelement is displayed
		System.out.println("checkbox 3 Displayed : "+check3.isDisplayed());
		
		//isEnabled -- checks if the web element is clickable
		//isEnabled --- false -- if web element is not clickable
		System.out.println("checkbox 3 Enabled : "+check3.isEnabled());		
		
		//isSelected -- works when we click on webelement
		System.out.println("checkbox 3 Before :"+check3.isSelected());		//false
		check3.click();
		System.out.println("checkbox 3 After :"+check3.isSelected());		//true
		
		
		
		//checking isSelected if we click on same checkbox again. It will clear the checkbox
		Thread.sleep(3000);
		check1.click();
		System.out.println("Check box 1 after :"+check1.isSelected());
		
		Thread.sleep(3000);
		check2.click();
		System.out.println("Check box 2 after :"+check2.isSelected());
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		CheckboxButtonInSelenium c1=new CheckboxButtonInSelenium();
		c1.getCheckbox();
	}

}
