package testng;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DataProviderTest3 {
	
	//Providing data from Another Class using DataProvider
	WebDriver driver;
	String path="";
	String actual_driver_path="";

	@BeforeMethod
	public void setup()
	{
		path=System.getProperty("user.dir");
		actual_driver_path=path+"\\Driver\\chromedriver.exe";
		driver=new ChromeDriver();
		driver.get("https://www.google.com/");
	}
	
	@Test(dataProvider = "test-search-data",dataProviderClass = TestData.class)
	public void search(String keyword)
	{
		WebElement txt_search=driver.findElement(By.name("q"));
		txt_search.sendKeys(keyword);
		txt_search.sendKeys(Keys.ENTER);
	}
	
	@AfterMethod
	public void close() throws InterruptedException
	{
		Thread.sleep(5000);
		driver.close();
	}

}
