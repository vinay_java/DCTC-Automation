package testng;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AnnotationTest2 {
	
	@Test(priority =4)
	public void setup()
	{
		System.out.println("Inside Setup Method");
		//expecting true value but provided as false so test case will be failed
		Assert.assertTrue(false);
	}
	
	@Test(invocationCount = 5,priority = 3)
	public void login_to_page()
	{
		System.out.println("Inside Login Method");
	}

	@Test(priority = 1,dependsOnMethods = {"setup"})
	public void navigate_to_home_page()
	{
		System.out.println("Inside Home Method");
	}
	
	@Test(priority = 2)
	public void verify_registration_form()
	{
		System.out.println("Inside Registration Method");
	}
	
}
