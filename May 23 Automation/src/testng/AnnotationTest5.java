package testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AnnotationTest5 extends AnnotationTest4{
	
	@BeforeClass
	public void testAnotherclass()
	{
		System.out.println("Another Class 1");
	}
	
	@Test
	public void closure()
	{
		System.out.println("Inside Closure of Another Class");
	}
	
	@AfterClass
	public void testAnotherclass2()
	{
		System.out.println("Another Class 2");
	}

}
