package testng;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AnnotationTest3 {
	
	@Test(dependsOnGroups = {"Smoke"}, priority=1)
	public void setup()
	{
		System.out.println("Inside Setup Method");
	}
	
	@Test(groups = {"Smoke","Regression"},priority=4)
	public void login_to_page()
	{
		System.out.println("Inside Login Method");
		Assert.assertTrue(false);
	}

	@Test(groups = {"Smoke"}, priority=2)
	public void navigate_to_home_page()
	{
		System.out.println("Inside Home Method");
	}
	
	@Test(groups= {"Regression"}, priority=3)
	public void verify_registration_form()
	{
		System.out.println("Inside Registration Method");
	}
	
}
