package testng;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestAutomationUsingTestNG2 {
	
	
	WebDriver driver;
	String path="";
	String driver_path="";
	String expected_title="Facebook – log in or sign up";
	String actual_title="";
	
	@BeforeTest
	public void setup()
	{
		System.out.println("Opening Browser");
		path=System.getProperty("user.dir");
		driver_path=path+"\\Driver\\chromedriver.exe";
		driver=new ChromeDriver();
		driver.get("https://www.facebook.com/");
	}
	
	@Test(priority = 0)
	public void getTitle_of_webpage()
	{
		actual_title=driver.getTitle();
		Assert.assertEquals(actual_title, expected_title);
	}
	
	@Test(priority = 1)
	public void enterCredential() throws InterruptedException
	{
		Thread.sleep(3000);
		driver.findElement(By.id("email")).sendKeys("test@test.com");
		Thread.sleep(3000);
		driver.findElement(By.id("pass")).sendKeys("12345");
	}
	
	@AfterTest
	public void close() throws InterruptedException
	{
		Thread.sleep(3000);
		System.out.println("Closing Browser");
		driver.close();
	}
	
	

}
