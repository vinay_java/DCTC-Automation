package testng;

import org.testng.annotations.Test;

public class TestAutomation {

	@Test
	public void setup()
	{
		System.out.println("Inside Setup method");
	}
	
	@Test
	public void display()
	{
		System.out.println("Inside display method");
	}
	
	@Test
	public void getData()
	{
		System.out.println("Inside getData method");
	}
}
