package testng;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataProviderTest2 {
	
	WebDriver driver;
	String path="";
	String actual_driver_path="";
	
	@BeforeMethod
	public void setup()
	{
		path=System.getProperty("user.dir");
		actual_driver_path=path+"\\Driver\\chromedriver.exe";
		driver=new ChromeDriver();
		driver.get("https://www.facebook.com/");
	}
	
	@Test(dataProvider = "test-user-data",dataProviderClass =TestData.class)
	public void enterCredentials(String userName, String password) throws InterruptedException
	{
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(userName);
		WebElement pass = driver.findElement(By.id("pass"));
		pass.sendKeys(password);
		pass.sendKeys(Keys.ENTER);
		
		Thread.sleep(5000);
	}
	
	@AfterMethod
	public void close() throws InterruptedException
	{
		driver.close();
	}

}
