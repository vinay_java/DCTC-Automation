package testng;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestAutomationUsingTestNG {
	
	
	WebDriver driver;
	String path="";
	String driver_path="";
	String expected_title="Facebook – log in or sign up to Website";
	String actual_title="";
	
	@Test(priority = 0)
	public void setup()
	{
		path=System.getProperty("user.dir");
		driver_path=path+"\\Driver\\chromedriver.exe";
		driver=new ChromeDriver();
		driver.get("https://www.facebook.com/");
	}
	
	@Test(priority = 1)
	public void getTitle_of_webpage()
	{
		actual_title=driver.getTitle();
		Assert.assertEquals(actual_title, expected_title);
	}
	
	

}
