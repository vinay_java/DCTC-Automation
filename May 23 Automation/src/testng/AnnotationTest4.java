package testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AnnotationTest4 {
	
	@BeforeTest
	public void beforeTest()
	{
		System.out.println("Before Test Annotation");
	}
	
	@BeforeClass
	public void testBeforeClass()
	{
		System.out.println("Before Class Annotation");
	}
	
	@BeforeSuite
	public void testBeforeSuite()
	{
		System.out.println("Before Suite Annotation");
	}
	
	@BeforeMethod
	public void beforeMethod()
	{
		System.out.println("Before Method Annotation");
	}
	
	@Test
	public void setup()
	{
		System.out.println("Inside Setup Test Case");
	}
	
	@Test
	public void exit()
	{
		System.out.println("Inside exit Test Case");
	}
	
	@AfterSuite
	public void testafterSuite()
	{
		System.out.println("After Suite Annotation");
	}
	@AfterClass
	public void testAfterClass()
	{
		System.out.println("After Class Annotation");
	}
	
	@AfterTest
	public void afterTest()
	{
		System.out.println("After Test Annotation");
	}
	
	@AfterMethod
	public void afterMethod()
	{
		System.out.println("After Method Annotation");
	}
	
}



