package testng;

import org.testng.annotations.DataProvider;

public class TestData {
	
	
	@DataProvider(name = "test-search-data")
	public static Object[][] dataProvider()
	{
		Object x[][]= {
				{"Tutorials on Automation Testing"},{"Selenium 3.0 Vs Selenium 4.0"}	
		};
		return x;
	}
	
	
	@DataProvider(name="test-user-data")
	public static Object[][] dataProviderEmailandPassword()
	{
		Object x[][]= {
				{"pooja@test.com","12345"},{"ravindra@test.com","97557989"}
				
		};
		return x;
	}

}
