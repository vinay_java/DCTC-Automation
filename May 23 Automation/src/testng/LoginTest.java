package testng;

import org.testng.annotations.Test;

public class LoginTest {
	
	@Test(priority = 0)
	public void setup()
	{
		System.out.println("Inside Setup Method");
	}
	
	@Test(priority = 1)
	public void login_to_page()
	{
		System.out.println("Inside Login Method");
	}

	@Test(priority = 2)
	public void navigate_to_home_page()
	{
		System.out.println("Inside Home Method");
	}
	
	@Test(priority = 3)
	public void verify_registration_form()
	{
		System.out.println("Inside Registration Method");
	}
	
	@Test(priority = 5)
	public void cleanup()
	{
		System.out.println("Inside Cleanup Method");
	}
	
	@Test(priority = 4)
	public void verify_title_of_page()
	{
		System.out.println("Inside Title of Page Method");
	}
	
}
