import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ActionsDemo3 {

	WebDriver driver;		//class variable
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		ActionsDemo3 ad=new ActionsDemo3();
		ad.setup();
		ad.drag_n_drop();
	}
	
	public void setup()
	{
		System.setProperty("web.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.guru99.com/test/drag_drop.html");
	}
	
	public void drag_n_drop() throws InterruptedException
	{	
		WebElement btn_drag_amount=driver.findElement(By.id("fourth"));
		WebElement drop_amount=driver.findElement(By.id("amt7"));
		
		Actions act=new Actions(driver);
		act.dragAndDrop(btn_drag_amount, drop_amount).build().perform();
		
		Thread.sleep(3000);
		WebElement drop_amount_credit=driver.findElement(By.id("amt8"));
		act.dragAndDrop(btn_drag_amount, drop_amount_credit).build().perform();
		
		
	}
}











