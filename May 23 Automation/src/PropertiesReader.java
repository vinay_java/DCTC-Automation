import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PropertiesReader {

	WebDriver driver;
	String path="";
	String actual_file_path="";
	String actual_driver_path="";
	String base_url="";
	String email="";
	String password="";
	public void readData() throws IOException
	{
		path=System.getProperty("user.dir");
		actual_file_path=path+"\\Resource\\Test.properties";
		
		File f=new File(actual_file_path);
		FileInputStream fin=new FileInputStream(f);
		
		Properties p=new Properties();
		p.load(fin);
		
		//reading values from properties file
		base_url=p.getProperty("url");		
		email= p.getProperty("email");
		password=p.getProperty("password");
		
	}
	
	public void setup()
	{
		actual_driver_path=path+"\\Driver\\chromedriver.exe";
		System.setProperty("web.chrome.driver", actual_driver_path);
		
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(base_url);
		
	}
	
	public void sendCredentials()
	{
		driver.findElement(By.id("email")).sendKeys(email);
		driver.findElement(By.id("pass")).sendKeys(password);
		
	}
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		PropertiesReader e=new PropertiesReader();
		e.readData();
		e.setup();
		e.sendCredentials();
	}

}
