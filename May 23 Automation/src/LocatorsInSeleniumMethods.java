import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorsInSeleniumMethods {

	//created a function/method without parameter
	public void setup() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("https://www.amazon.in/?&ext_vrnc=hi&tag=googhydrabk1-21&ref=pd_sl_7hz2t19t5c_e&adgrpid=58355126069&hvpone=&hvptwo=&hvadid=610644601173&hvpos=&hvnetw=g&hvrand=16620686416428612981&hvqmt=e&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9303436&hvtargid=kwd-10573980&hydadcr=14453_2316415");
		
		
		WebElement img=driver.findElement(By.xpath("//img[@alt='Up to 60% off on everything cricket']//parent::div//parent::a"));
		System.out.println(img.getAttribute("aria-label"));
		
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		LocatorsInSeleniumMethods loc=new LocatorsInSeleniumMethods(); //create an object of class
		loc.setup();
	}
	

}
