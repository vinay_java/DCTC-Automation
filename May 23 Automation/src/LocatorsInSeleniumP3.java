import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorsInSeleniumP3 {

	//created a function/method without parameter
	public void setup() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("https://www.facebook.com/");
		
		
		//actual xpath
		WebElement email=driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/form/div[1]/div[1]/input"));
		email.sendKeys("test@test.com");
		
		WebElement pass=driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/form/div[1]/div[2]/div[1]/input"));
		pass.sendKeys("12345");
		
		Thread.sleep(3000);		//delayed the execution by 03 seconds
		email.clear();			//clear the textfield of email
		
		Thread.sleep(3000);		//delayed the execution by 03 seconds
		pass.clear();			//clear the textfield of password
		
		//relative xpath
		WebElement email_relative=driver.findElement(By.xpath("//input[@data-testid='royal_email']"));
		email_relative.sendKeys("vinay@gmail.com");
		
		WebElement pass_relative=driver.findElement(By.xpath("//input[@name='pass']"));
		pass_relative.sendKeys("795466");
		
	}
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		LocatorsInSeleniumP3 loc=new LocatorsInSeleniumP3(); //create an object of class
		loc.setup();
	}
	

}
