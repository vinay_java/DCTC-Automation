import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExelReader {

	String path="";
	String actual_file_path="";
	
	public void readData() throws IOException
	{
		path=System.getProperty("user.dir");
		actual_file_path=path+"\\Resource\\TestEmp.xlsx";
		
		File f=new File(actual_file_path);
		FileInputStream fis=new FileInputStream(f);
		
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		XSSFSheet sheet=workbook.getSheet("EmpData");
		
		String emp_label= sheet.getRow(0).getCell(0).getStringCellValue();
		String emp_value= sheet.getRow(0).getCell(1).getStringCellValue();
		
		System.out.println(emp_label+" : "+emp_value);
		
		String doj_label=sheet.getRow(4).getCell(0).getStringCellValue();
		Date doj_value = sheet.getRow(4).getCell(1).getDateCellValue();
		
		System.out.println(doj_label+":"+doj_value);
		
		String join_label=sheet.getRow(5).getCell(0).getStringCellValue();
		boolean join_value = sheet.getRow(5).getCell(1).getBooleanCellValue();
		
		System.out.println(join_label+":"+join_value);
		
		String contact_label=sheet.getRow(3).getCell(0).getStringCellValue();
		int contact_value = (int) sheet.getRow(3).getCell(1).getNumericCellValue();
		
		System.out.println(contact_label+":"+contact_value);
		
		System.out.println(sheet.getPhysicalNumberOfRows());
		
		System.out.println(sheet.getLastRowNum());
	}
	
	public void getAllData() throws IOException
	{
		File f=new File(actual_file_path);
		FileInputStream fis=new FileInputStream(f);
		
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		XSSFSheet sheet=workbook.getSheet("Emp");
		
		int n=sheet.getPhysicalNumberOfRows();
		for(int i=0;i<n;i++)
		{
			String label=sheet.getRow(i).getCell(0).getStringCellValue();
			String value=sheet.getRow(i).getCell(1).getStringCellValue();
			
			System.out.println(label+":"+value);
		}
	}
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		ExelReader e=new ExelReader();
		e.readData();
		e.getAllData();
	}

}
