import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class RadioButtonInSelenium {


	public void getRadio()
	{
		System.setProperty("web.chrome.driver", "C:\\Users\\ip-slim-3\\Git DCTC\\DCTC-Automation\\May 23 Automation\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://demo.guru99.com/test/radio.html");
		
		WebElement radio1= driver.findElement(By.id("vfb-7-1"));
		System.out.println(radio1.getAttribute("value"));
		//isDisplayed -- when the webelement is displayed
		System.out.println("Radio 1 Displayed : "+radio1.isDisplayed());
		
		//isEnabled -- checks if the web element is clickable
		//isEnabled --- false -- if web element is not clickable
		System.out.println("Radio 1 Enabled : "+radio1.isEnabled());		
		
		//isSelected -- works when we click on webelement
		System.out.println("Radio 1 Before :"+radio1.isSelected());		//false
		radio1.click();
		System.out.println("Radio 1 After :"+radio1.isSelected());		//true
		
		
		WebElement radio2= driver.findElement(By.id("vfb-7-2"));
		System.out.println(radio2.getAttribute("value"));
		//isDisplayed -- when the webelement is displayed
		System.out.println("Radio 2 Displayed : "+radio2.isDisplayed());
		
		//isEnabled -- checks if the web element is clickable
		//isEnabled --- false -- if web element is not clickable
		System.out.println("Radio 2 Enabled : "+radio2.isEnabled());		
		
		//isSelected -- works when we click on webelement
		System.out.println("Radio 2 Before :"+radio2.isSelected());		//false
		radio2.click();
		System.out.println("Radio 2 After :"+radio2.isSelected());		//true
		
		
		WebElement radio3= driver.findElement(By.id("vfb-7-3"));
		System.out.println(radio3.getAttribute("value"));
		//isDisplayed -- when the webelement is displayed
		System.out.println("Radio 3 Displayed : "+radio3.isDisplayed());
		
		//isEnabled -- checks if the web element is clickable
		//isEnabled --- false -- if web element is not clickable
		System.out.println("Radio 3 Enabled : "+radio3.isEnabled());		
		
		//isSelected -- works when we click on webelement
		System.out.println("Radio 3 Before :"+radio3.isSelected());		//false
		radio3.click();
		System.out.println("Radio 3 After :"+radio3.isSelected());		//true
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		RadioButtonInSelenium r1=new RadioButtonInSelenium();
		r1.getRadio();
	}

}
