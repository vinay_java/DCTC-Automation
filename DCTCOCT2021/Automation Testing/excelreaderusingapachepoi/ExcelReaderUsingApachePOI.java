package excelreaderusingapachepoi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ExcelReaderUsingApachePOI {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		File f = new File(System.getProperty("user.dir") + "\\Resource\\DCTC.xlsx");
		FileInputStream fis = new FileInputStream(f);

		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		
		XSSFRow row=null;
		// XSSFSheet sheet=workbook.getSheet("Sheet1");

		XSSFSheet sheet = workbook.getSheetAt(0);

		String data = sheet.getRow(0).getCell(0).getStringCellValue();

		System.out.println(data);

		String value = sheet.getRow(0).getCell(1).getStringCellValue();

		System.out.println(value);

		System.out.println(sheet.getRow(0).getLastCellNum());
		
		System.out.println(sheet.getLastRowNum());

		System.out.println(sheet.getPhysicalNumberOfRows());
		
		row=sheet.getRow(5);
		
		XSSFCell cell=row.getCell(1);
		
		//boolean x=cell.getBooleanCellValue();
		
		//System.out.print(x);
		
		System.out.println(cell.getBooleanCellValue());
	}

}
