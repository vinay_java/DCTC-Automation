package excelreaderusingapachepoi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ExcelReaderUsingApachePOIAllData {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		File f = new File(System.getProperty("user.dir") + "\\Resource\\DCTC.xlsx");
		FileInputStream fis = new FileInputStream(f);

		//ArrayList<String> row_key = new ArrayList();

		//ArrayList<String> row_value = new ArrayList();

		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheetAt(0);
		
		int num_of_rows=sheet.getPhysicalNumberOfRows();
		
		for(int i=1;i<num_of_rows-1;i++)
		{
			String key=sheet.getRow(i).getCell(0).getStringCellValue();
			String value =sheet.getRow(i).getCell(1).getStringCellValue();
			
			System.out.println(key+" : "+value);

			// row_key.add(sheet.getRow(i).getCell(0).getStringCellValue());

			//	row_value.add(sheet.getRow(i).getCell(1).getStringCellValue());
		}
		
		//	System.out.println(row_key);
		//System.out.println(row_value);
	}

}
