package actionclassinselenium;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class RobotClassInSelenium {

	public static void main(String[] args) throws AWTException, InterruptedException {
		// TODO Auto-generated method stub
		String path=System.getProperty("user.dir");
		String actualpath=path+"\\Driver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", actualpath);
		
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get("https://demo.guru99.com/test/upload/");
		
		Actions ac=new Actions(driver);
		
		ac.click(driver.findElement(By.id("uploadfile_0"))).build().perform();
		
		StringSelection strSelect=new StringSelection("C:\\Users\\ip-slim-3\\eclipse-workspace\\DCTCOCT2021\\Resource\\HDFC.pdf");
		
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSelect, null);
		
		Robot r=new Robot();
		
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_CONTROL);
		
		Thread.sleep(3000);
		
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);
		
		Thread.sleep(3000);
		
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);
		
		Thread.sleep(3000);
		
		
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		
		Thread.sleep(3000);
		
		driver.findElement(By.id("terms")).click();
		
		Thread.sleep(3000);
		
		
		driver.findElement(By.id("submitbutton")).click();
		
		
	}

}
