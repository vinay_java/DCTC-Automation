package datetimepickerinselenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DatePickerInSelenium2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		String path=System.getProperty("user.dir");
		String actualpath=path+"\\Driver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", actualpath);
		
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get("https://demoqa.com/date-picker");
		
		driver.findElement(By.id("dateAndTimePickerInput")).click();
		
		Thread.sleep(3000);
		
		driver.findElement(By.className("react-datepicker__month-read-view--down-arrow")).click();
	
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@class='react-datepicker__month-dropdown']//div[6]")).click();
		
		Thread.sleep(3000);
		
		driver.findElement(By.className("react-datepicker__year-read-view--down-arrow")).click();
		
		Thread.sleep(3000);
	
		//driver.findElement(By.className("react-datepicker__navigation react-datepicker__navigation--years react-datepicker__navigation--years-previous")).click();
		
		driver.findElement(By.xpath("//*[@class='react-datepicker__year-dropdown']//div[11]")).click();
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@class='react-datepicker__month']//div[contains(text(),'21')]")).click();
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@class='react-datepicker__time-list']//li[contains(text(),'6')]")).click();
	}

}
