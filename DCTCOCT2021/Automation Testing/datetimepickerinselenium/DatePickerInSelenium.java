package datetimepickerinselenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DatePickerInSelenium {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		String path=System.getProperty("user.dir");
		String actualpath=path+"\\Driver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", actualpath);
		
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get("https://demoqa.com/date-picker");
		
		driver.findElement(By.id("datePickerMonthYearInput")).click();
		
		WebElement month = driver.findElement(By.xpath("//*[@class='react-datepicker__month-select']"));
		
		Select sel_month=new Select(month);
		
		Thread.sleep(3000);
		sel_month.selectByIndex(3);
		
		WebElement year=driver.findElement(By.xpath("//*[@class='react-datepicker__year-select']"));
		
		Select sel_year=new Select(year);
		
		Thread.sleep(3000);
		
		sel_year.selectByValue("1906");
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@class='react-datepicker__month']//*[contains(text(),'25')]")).click();
	}

}
