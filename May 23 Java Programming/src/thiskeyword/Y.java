package thiskeyword;

//passing this as parameter in method

public class Y {

	int a, b;		//class variables
	
	//parameterised constructor
	public Y(int a, int b)
	{
		this.a=a;
		this.b=b;
		
	}
	
	public void test(Y y2)
	{
		System.out.println("In Test Method");
		System.out.println(y2.a +","+y2.b);
	}
	
	public void show()
	{
		System.out.println("In Show Method");
		test(this);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Y y1=new Y(12,5);		//call to parameterised constructor
		y1.show();
		
		
	}

}



