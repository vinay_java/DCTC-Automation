package thiskeyword;

public class Employee {
	
	String emp_name="";
	int emp_id;
	public Employee()
	{
		System.out.println("Inside Default Constructor");
	}
	
	public Employee(Account a2)
	{
		System.out.println("Inside Parameterised Constructor");
		this.emp_id=45627;
		this.emp_name="Akshay";
		System.out.println(a2.x+","+a2.y);
	}
	
	public void getData()
	{
		
	}
	
	public static void main(String[] args) {
		Employee e1=new Employee();
	}

}
