package thiskeyword;

public class A {

	int x, y;		//class variables
	
	//Default Constructor
	public A()
	{
		x=45;
		y=2;
	}
	
	
	//parameterised constructor
	public A(int x, int y)
	{
		this.x=x;
		this.y=y;
	}
	
	public void multiply()
	{
		int z=x * y;
		System.out.println("Multiplication : "+z);
		this.divide();			//this is calling the current class methods
	}
	
	public void divide()
	{
		int d=x / y;
		System.out.println("Division :"+d);
		this.square();
	}
	
	public void square()
	{
		int s=x * x;
		System.out.println("Square is : "+s);
	}
	
	//call to static method
	public static void average()
	{
		A a3=new A();				//call to default constructor
		int m=(a3.x + a3.y)/2;		//accessing class variables for a3 object
		System.out.println("Average :"+m);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

			
		A a1=new A();	//call to default constructor
		a1.multiply();
		average();				//no need of object as its static method
		
		A a2=new A(25,4);		//call to parameterised construtor
		a2.multiply();
	}

}



