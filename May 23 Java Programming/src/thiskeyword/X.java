package thiskeyword;

//call to constructor of class

public class X {

	int a, b;		//class variables
	
	//Default Constructor
	public X()
	{
		//this(25,3);		//call to parameterised constructor
		a=20;
		b=7;
		multiply();
	}
	
	//parameterised constructor
	public X(int a, int b)
	{
		this();
		this.a=a;
		this.b=b;
		multiply();
	}
	
	public void multiply()
	{
		int z=a * b;
		System.out.println("Multiplication : "+z);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		X x1=new X(12,5);		//call to parameterised constructor
	}

}



