package interfacedemo;

import polymorphism.HR;

public class Employee extends HR implements Org {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Employee e1=new Employee();
		e1.testData();
		e1.getHR();
		e1.tellMe();
		
		//Hiding details-- Abstraction
		HR h1=new Employee();
		h1.getHR();
				
		Org o=new Employee();
		o.testData();
		
	}

	public void tellMe()
	{
		System.out.println("Tell Me");
	}
	
	@Override
	public void testData() {
		// TODO Auto-generated method stub
		
		System.out.println("Inside Test Data - "+z);
	}

}
