package interfacedemo;

public interface Org {
	
	//the method in interface is abstract and variable will be final
	//methods will be abstract -- no method body
	//we cant have constructors for interface
	//we cant create object of Interface
	
	public void testData();
	int z=55;
}

