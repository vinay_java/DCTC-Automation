package interfacedemo;

public class Test implements Org, Policy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//reference of child and object of child
		Test t1=new Test();
		t1.test1();
		t1.test2();
		t1.testData();
		
		//reference of Parent and object of child
		Policy p1=new Test();
		p1.test1();
		p1.test2();
		
		//reference of Parent and object of child
		Org o=new Test();
		o.testData();
	}

	@Override
	public void test1() {
		// TODO Auto-generated method stub
		System.out.println("Test 1");
	}

	
	@Override
	public void testData() {
		// TODO Auto-generated method stub
		System.out.println("Test Data");
	}

	@Override
	public void test2() {
		// TODO Auto-generated method stub
		System.out.println("Test 2");
	}
}






