package abstractdemo;

public abstract class Abc {
	
	int z=55;
	//we can have constructor of abstract class
	public Abc(int a1)
	{
		System.out.println("Inside Abc class");
	}
	
	public abstract void show();
	public abstract void test();
	
	//we can have method with method body in abstract class
	public void showDetails()
	{
		System.out.println("In Show details");
	}
}