package oops;

public class Employee {

	int emp_id;
	int emp_contact;
	String emp_name;
	
	//Parameterised Constructor
	public Employee(int x,int y,String z)
	{
		emp_id=x;
		emp_contact=y;
		emp_name=z;
	}
		
	
	public void showData()
	{
		System.out.println("Emp ID - "+emp_id);
		System.out.println("Emp Contact - "+emp_contact);
		System.out.println("Emp Name - "+emp_name);
	}
	
	public static void main(String[] args) {
		Employee e1=new Employee(12345,98855,"Ajay");
		e1.showData();
		
		Employee e2=new Employee(12355, 98877, "Sujay");
		e2.showData();
		
		Employee e3=new Employee(12377, 98823, "Vijay");
		e3.showData();
		
	}
}


