package oops;

public class TestConstructor {

	int e;		//class variable ---its declared in class
	
	//Default Constructor
	public TestConstructor()
	{
		//assign values to class variable
		e=10;
		System.out.println("Inside Default Constructor -"+e);
	}
	
	public void testData()
	{
		int y;		//local variable
		y=54;
		System.out.println("Inside method Testdata - "+(y*e));
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
		TestConstructor t1=new TestConstructor();	//object of class
		t1.testData();
		
		TestConstructor t2=new TestConstructor();
		t2.e=12;			//we are assigning value to e
		t2.testData();
		
		TestConstructor t3=new TestConstructor();	
		t3.e=7;
		t3.testData();
		
		System.out.println("Value of e with t1 : "+t1.e);
		System.out.println("Value of e with t2 : "+t2.e);
		System.out.println("Value of e with t3 : "+t3.e);
	}
	
}
