package oops;

public class Account {

	int acc_id;
	
	public Account(int acc_id)
	{
		this.acc_id=acc_id;
	}

	public void showDetails()
	{
		System.out.println("Account ID - "+acc_id);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Account a1=new Account(7777);
		a1.showDetails();
		
		Account a2=new Account(5555);
		a2.showDetails();
	}

}
