
public class Brushup3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//We want to print values from 1 to 20
		//only even numbers
		
		for(int i=1;i<=20;i++)
		{
			if(i%2==0)
			{
				System.out.println(i);
			}
		}
		
		
		//Printing Even numbers from Array
		int x[]=new int[5];
		x[0]=77;
		x[1]=23;
		x[2]=12;
		x[3]=14;
		x[4]=55;
		
		for(int i=0;i<x.length;i++)
		{
			
			if(x[i]%2==0)
			{
				System.out.println("Even : "+x[i]);
			}
			else
			{
				System.out.println("Odd : "+x[i]);
			}
		}
		
		
	}

}
