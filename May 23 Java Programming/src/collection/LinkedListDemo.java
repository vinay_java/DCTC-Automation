package collection;

import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LinkedListDemo a=new LinkedListDemo();
		a.getLinkedList();
	}
	
	public void getLinkedList()
	{
		LinkedList<Integer> al=new LinkedList();
	
		al.add(new Integer(45));
		al.add(new Integer(65));
		al.add(new Integer(56));
		
		System.out.println(al);
		
		al.add(0,new Integer(12));
		System.out.println(al);
		
		al.add(2,new Integer(12));
		System.out.println(al);
		
		al.remove(new Integer(12));
		System.out.println(al);
		
		al.remove(2);
		System.out.println(al);
		
		al.add(null);
		System.out.println(al);
	}
}
