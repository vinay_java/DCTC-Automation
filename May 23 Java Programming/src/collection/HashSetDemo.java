package collection;

import java.util.HashSet;

public class HashSetDemo {
	
	public static void main(String[] args) {
		HashSetDemo h=new HashSetDemo();
		h.getHashSet();
		h.getHashSetDouble();
	}
	
	public void getHashSet()
	{
		HashSet<String> hs=new HashSet();
		hs.add("Hello");
		hs.add(new String("Hi"));
		hs.add("Ajay");
		hs.add("Vinay");
		hs.add("Pooja");
		hs.add("Nayana");
		hs.add(null);
		hs.add("Ajay");
		System.out.println(hs);
		
		hs.remove("Hello");
		System.out.println(hs);
		

	}
	
	public void getHashSetDouble()
	{
		HashSet<Double> hs=new HashSet();
		hs.add(new Double(58.45));
		hs.add(new Double(65.85));
		hs.add(new Double(78));
		hs.add(null);
		hs.add(new Double(78));
		System.out.println(hs);
		
		hs.remove(new Double(65.85));
		System.out.println(hs);
	}
	
}
