
public class Brushup2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Arrays of Int
		int num[]=new int[5];		//one dimensional array
		
		//array starts from 0 index
		num[0]=12;
		num[1]=23;
		num[2]=45;
		num[3]=54;
		num[4]=78;
		
		System.out.println(num[2]);
		System.out.println(num[0]);
		
		for(int j=0;j<num.length;j++)
		{
			System.out.println(num[j]);
		}
		
		
		//Array of String
		String city[]=new String[7];
		city[0]="Pune";
		city[1]="Mumbai";
		city[2]="Bangalore";
		city[3]="Delhi";
		city[4]="Jaipur";
		city[5]="Kolkata";
		city[6]="Chennai";
		
		for(int i=0;i<city.length;i++)
		{
			System.out.println("City is ="+city[i]);
		}
	}

}








