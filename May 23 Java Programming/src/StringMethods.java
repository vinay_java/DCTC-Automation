
public class StringMethods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String emp=new String("Ajay");	//Creating Object
		
		String emp_name="Vinay";		//literals
		
		System.out.println(emp_name.toUpperCase());
		
		String empaddress="Near&Trends,206 South?West&Pune";
		String result[]=empaddress.split("&");
		
		for(int i=0;i<result.length;i++)
		{
			System.out.println(result[i]);
		}
		
		String result1[]=empaddress.split("[& , ?]");
		for(int i=0;i<result1.length;i++)
		{
			System.out.println(result1[i]);
		}
		
		
		String y="Object Oriented Programming";
		String z[]=y.split("Oriented");
		
		for(int i=0;i<z.length;i++)
		{
			System.out.println(z[i].trim());
		}
		
		
		//contains
		
		String w="We Are Indians";
		System.out.println(w.contains("We"));
	}

}
