
public class WrapperDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		int i=85;					//i is a variable of type int
		Integer x=new Integer(i);	//x is an Object
		
		System.out.println(x);
		
		Integer z=Integer.valueOf(i);		//converting primitive data type to an object
		System.out.println("Value in : "+z);
		
		char w='e';						//w is a variable
		Character q=new Character(w);	//q is an Object
				 
		Character p= Character.valueOf(w);
		
		double f=36.45;					//f is a variable
		Double d=new Double(f);			//d is an Object
		
		Double m=Double.valueOf(f);
		
		
		int a=45, b=56;
		
		int n=a + b;
		System.out.println("Addition is :"+n);
		
		String h=Integer.toString(a) + 	Integer.toString(b);
		System.out.println(h);
		
		String str="17";					//string type of object
		String str1="18";
		String str2=str + str1;
		System.out.println(str2);
		
		Integer q1= Integer.valueOf(str);	//converting string to integer type of object
		Integer q2=Integer.valueOf(str1);
		
		Integer q3= q1 + q2;
		System.out.println(q3);
		
	}			

}





