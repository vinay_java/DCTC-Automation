package polymorphism;

//method overloading
public class Employee {
	int emp_id;
	String emp_name;
	public Employee()
	{
		emp_id=1344;
		emp_name="Vinay";
	}
	
	public Employee(int x,String y)
	{
		emp_id=x;
		emp_name=y;
	}
	
	
	public void show()
	{
		System.out.println("Inside show without parameter");
		System.out.println(emp_id+","+emp_name);
	}
	
	public void show(int a)
	{
		System.out.println("Inside show int: "+a);
	}

	public void show(double a)
	{
		System.out.println("Inside show double: "+a);
	}

	public void show(int a, int b)
	{
		System.out.println("Inside show 02 int: "+a+","+b);
	} 
	
	public void get()
	{
		System.out.println("In get method");
	}
	
	public static void main(String[] args) {
		
		Employee e1=new Employee();
		e1.show(15, 25);
		e1.show(35.0);
		e1.show(85.78);
		e1.show();
		
		Employee e2=new Employee(2500, "Ajay");
		e2.show();
	}
}






