package polymorphism;

public class Account extends Employee {
	
	
	@Override
	public void show()
	{
		super.show();
		System.out.println("Hello ");
	}
	
	@Override
	public void show(int z)
	{
		System.out.println("Hi there");
	}
	
	public void testData()
	{
		System.out.println("In test data");
	}
	public static void main(String[] args) {
		Account a1=new Account();
		a1.show();
		a1.show(25);
		a1.show(25.35);
		a1.show(65, 75);
	}
}





