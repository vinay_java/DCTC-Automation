package polymorphism;

public class HR extends Account{
	
	public void getHR()
	{
		System.out.println("Inside HR method");
	}
	
	public static void main(String[] args) {
		HR h1=new HR();
		h1.getHR();
		h1.get();
		h1.testData();
	}

}
