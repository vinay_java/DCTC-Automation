package inheritance;

public class A {
	
	int x,y;		
	
	public A()
	{
		x=12;
		y=5;
		System.out.println("Inside Default Constructor of Parent");
	}
	public A(int x, int y)
	{
		this.x=x;
		this.y=y;
		System.out.println("Inside parameterised constructor of Parent");
	}
	
	public void getData()
	{
		int z=x * y;
		System.out.println("In getdata method of Parent Class - "+z);
	}
}
