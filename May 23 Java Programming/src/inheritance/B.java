package inheritance;

//single inheritance 
public class B extends A{

	
	int m,n; 
	public B()
	{
		super(20,7);
		m=12;
		n=57;
		System.out.println("Inside Child Class Constructor");
	}
	
	public B(int y, int z)
	{
		System.out.println("Inside Child Class Parameterised Constructor");
	}
	
	public void showData()
	{
		System.out.println("Inside Show Data of Child Class");
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		B b1=new B();		//called the constructor of child and 
							//automatic call given to parent constructor
		b1.getData();
		b1.showData();
		
		B b2=new B(45, 75);
		b2.getData();
		b2.showData();
	}

}






