package com.guru99.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NewCustomerPage extends BasePage{

	@CacheLookup
	@FindBy(name ="city")
	private WebElement city_name;
	
	public NewCustomerPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public void addNewCustomer()
	{
		elementControl.setText(city_name, "Pune");
	}
}
