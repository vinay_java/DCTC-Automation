package commonLibs.Implementation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CommonDriver {
	
	private WebDriver driver;
	private int pageLoadTimeout;
	private int elementDetectionTImeout;
	private String currentWorkingDirectory;
	public CommonDriver(String browserType) throws Exception {
		// TODO Auto-generated constructor stub
		pageLoadTimeout=10;
		elementDetectionTImeout=10;
		
		currentWorkingDirectory=System.getProperty("user.dir");
		
		if(browserType.equalsIgnoreCase("chrome"))
		{
			WebDriverManager.chromedriver().setup();
			//System.setProperty("webdriver.chrome.driver", currentWorkingDirectory+"\\Driver\\chromedriver.exe");
			driver=new ChromeDriver();
		}
		else if(browserType.equalsIgnoreCase("edge"))
		{
			WebDriverManager.edgedriver().setup();
			//System.setProperty("webdriver.edge.driver",currentWorkingDirectory+"\\Driver\\msedgedriver.exe");
			driver=new EdgeDriver();
		}
		else
		{
			throw new Exception("Invalid Browser Type"+browserType);
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
	}
	
	public void navigateToURL(String url)
	{
		driver.manage().timeouts().pageLoadTimeout(pageLoadTimeout, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(elementDetectionTImeout, TimeUnit.SECONDS);
		
		driver.get(url);
	}
	
	public WebDriver getDriver() {
		return driver;
	}

	public void setPageLoadTimeout(int pageLoadTimeout) {
		this.pageLoadTimeout = pageLoadTimeout;
	}

	public void setElementDetectionTImeout(int elementDetectionTImeout) {
		this.elementDetectionTImeout = elementDetectionTImeout;
	}
	
	public void closeBrowser()
	{
		driver.close();
	}
	
	public void closeAllBrowser()
	{
		driver.quit();
	}
	public String getTitle()
	{
		return driver.getTitle();
	}

}
