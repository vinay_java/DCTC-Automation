package com.guru99.tests;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

public class AddNewCustomerTest extends LoginTests{
	
	@Test
	public void verifyAddNewCustomer() throws InterruptedException
	{
		reportUtils.createATestCase("verify Add New Customer");
		reportUtils.addTestLog(Status.INFO, "Performing Log");
		
		newcustpage.addNewCustomer();
		Thread.sleep(5000);
		
		reportUtils.addTestLog(Status.INFO, "Comparing expected and Actual Title");
		//Assert.assertEquals(actualTitle, expectedTitle);
	}
}