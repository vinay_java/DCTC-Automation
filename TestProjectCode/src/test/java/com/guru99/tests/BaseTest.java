package com.guru99.tests;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.Status;
import com.guru99.pages.HomePage;
import com.guru99.pages.LoginPage;
import com.guru99.pages.NewCustomerPage;

import commonLibs.Implementation.CommonDriver;
import commonLibs.utils.ConfigUtils;
import commonLibs.utils.ReportUtils;
import commonLibs.utils.ScreenshotUtils;

public class BaseTest {
	
	CommonDriver cmnDriver; 
	String url;
	WebDriver driver;
	LoginPage loginpage;
	HomePage homepage;
	NewCustomerPage newcustpage;
	
	String currentWorkingDirectory;
	String configFileName;
	Properties configProperty;
	
	ReportUtils reportUtils;
	String reportFilename;
	
	ScreenshotUtils screenshot;
	
	@BeforeSuite
	public void preSetup() throws IOException
	{
		currentWorkingDirectory=System.getProperty("user.dir");
		configFileName=currentWorkingDirectory+"\\Config\\config.properties";
		reportFilename=currentWorkingDirectory+"\\Reports\\guru99TestReport.html";
		
		configProperty=ConfigUtils.readProperties(configFileName);
		
		reportUtils=new ReportUtils(reportFilename);
	}
	@BeforeClass
	public void setup() throws Exception
	{
		url=configProperty.getProperty("baseUrl");
		
		String browserType=configProperty.getProperty("browserType");
		
		cmnDriver=new CommonDriver(browserType);
		
		driver=cmnDriver.getDriver();
		
		loginpage=new LoginPage(driver);
		
		homepage=new HomePage(driver);
		
		newcustpage=new NewCustomerPage(driver);
		
		screenshot=new ScreenshotUtils(driver);
		
		cmnDriver.navigateToURL(url);
	}
	
	@AfterClass
	public void tearDown()
	{
		cmnDriver.closeAllBrowser();
	}
	
	@AfterSuite
	public void postTearDown()
	{
		reportUtils.flushReport();
	}
	
	@AfterMethod
	public void postTestAction(ITestResult result) throws Exception
	{
		String testcasename=result.getName();
		long executionTime=System.currentTimeMillis();
		
		String screenshotFileName=currentWorkingDirectory+"/screenshots/"+testcasename+executionTime+".png";
		
		if(result.getStatus()==ITestResult.FAILURE)
		{
			reportUtils.addTestLog(Status.FAIL, "One or More Steps Failes");
			screenshot.captureAndSavaeScreenshot(screenshotFileName);
			reportUtils.attachScreenshotToReport(screenshotFileName);
		}
	}

}
