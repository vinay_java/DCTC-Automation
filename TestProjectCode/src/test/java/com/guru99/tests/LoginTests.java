package com.guru99.tests;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

public class LoginTests extends BaseTest {
	
	@Parameters({"userId","userPassword"})
	@Test
	public void verifyUserLoginWithCorrectCredentials(String username, String password)
	{
		reportUtils.createATestCase("verify User Login With Correct Credentials");
		reportUtils.addTestLog(Status.INFO, "Performing Log");
		
		loginpage.loginToApplication(username, password);
		String expectedTitle="Guru99 Bank Manager HomePage";
		String actualTitle=cmnDriver.getTitle();
		
		reportUtils.addTestLog(Status.INFO, "Comparing expected and Actual Title");
		Assert.assertEquals(actualTitle, expectedTitle);
	}
	
	@Test
	public void verifyUserPage() throws InterruptedException
	{
		reportUtils.createATestCase("verify Click on New Customer");
		reportUtils.addTestLog(Status.INFO, "Performing Log");
		
		homepage.clickOnNewUser();
		Thread.sleep(3000);
		
		String expectedTitle="Guru99 Bank New Customer Entry Page";
		String actualTitle=cmnDriver.getTitle();
		
		reportUtils.addTestLog(Status.INFO, "Comparing expected and Actual Title");
		Assert.assertEquals(actualTitle, expectedTitle);
	}
	
	@Test
	public void verifyAddNewCustomer() throws InterruptedException
	{
		reportUtils.createATestCase("verify Add New Customer");
		reportUtils.addTestLog(Status.INFO, "Performing Log");
		
		newcustpage.addNewCustomer();
		Thread.sleep(5000);
		
		reportUtils.addTestLog(Status.INFO, "Comparing expected and Actual Title");
		//Assert.assertEquals(actualTitle, expectedTitle);
	}
	
}
